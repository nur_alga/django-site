from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

admin.site.site_header = "Страница админстратора"
admin.site.site_title = "Войти"
admin.site.index_title = "Добро пожаловать!"

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',  include('mainApp.urls')),
    url(r'^webexample/', include('webexample.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
