from django.db import models

class Sight(models.Model):
	title = models.CharField(u'Заголовок',max_length=120)
	text = models.TextField(u'Контент')
	date = models.DateTimeField(u'Дата публикаций')
	photo = models.ImageField(u'Фотография', upload_to='photos', blank=True)
	audio = models.FileField(u'Аудиофайлы',upload_to='audios', blank=True)
	
	def __str__ (self):
		return self.title