from django.conf.urls import url
from django.views.generic import ListView, DetailView
from webexample.models import Sight
urlpatterns = [
	url(r'^$', ListView.as_view(queryset=Sight.objects.all().order_by("-date")[:3],
		template_name="webexample/posts.html")),
	url(r'^(?P<pk>\d+)$', DetailView.as_view(model = Sight, template_name = "webexample/post.html")),
	url(r'^audio/(?P<pk>\d+)$', DetailView.as_view(model = Sight, template_name = "webexample/audio.html")),
	url(r'^audio/(?P<pk>\d+)$', DetailView.as_view(model = Sight, template_name = "webexample/audio.html")),
]