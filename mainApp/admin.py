from django.contrib import admin

from webexample.models import Sight

class SightAdmin(admin.ModelAdmin):
	list_display = ('title', 'date', 'photo', 'audio')
	list_filter = ('date',)
	search_fields = ('title',)

	def get_ordering(self, request):
		if request.user.is_superuser:
			return('title', 'date')
		return('title',)

admin.site.register(Sight,SightAdmin)
