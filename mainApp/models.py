from django.db import models

class ListOfSights(models.Model):
	title = models.CharField(max_length=120)
	text = models.TextField()
	date = models.DateTimeField()
	photo = models.ImageField(upload_to='photos', blank=True)

	def __str__ (self):
		return self.title

